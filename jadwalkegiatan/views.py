from django.shortcuts import render
from .forms import Message_Form
from django.http import HttpResponseRedirect
from .models import myActivity

# Create your views here.
def activity_form(request):
    if request.method == 'POST':
        forms = Message_Form(request.POST)
        if (forms.is_valid()):
            data = forms.cleaned_data
            myActivity.objects.create(
                activities=data['activity'],
                date=data['date'],
                time=data['time'],
                location=data['location'],
                category=data['category']
            )
            return HttpResponseRedirect('/tampilkanaktivitas')
    else:
        forms = Message_Form()

    content = {'form': forms}
    return render(request, 'jadwalkegiatan/jadwalkegiatan.html', content)

def activity_view(request) :
    if request.method =='POST':
        myActivity.objects.all().delete()
        return HttpResponseRedirect('/tampilkanaktivitas')
    list_activity = list(myActivity.objects.all().order_by("-id"))
    content = {'activity' : list_activity}
    return render(request, 'jadwalkegiatan/tampilanaktivitas.html', content)

