from django import forms


class Message_Form(forms.Form):
    activity = forms.CharField(label='Activity ', required=True, max_length=30, widget=forms.TextInput)
    date = forms.DateField(label='Date ', required=True,
                               widget=forms.DateTimeInput(attrs={'type': 'date'}))
    time = forms.TimeField(label='Time ', required=True,
                               widget=forms.DateTimeInput(attrs={'type': 'time'}))
    location = forms.CharField(label='Location ', required=True, max_length=50, widget=forms.TextInput)
    category = forms.CharField(label='Category ', required=True, max_length=30, widget=forms.TextInput)
